import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  texto = 'CLASE #1';
  constructor() {}
  onClick(){
    this.texto = 'Texto cambiado';
  }

}
